package routers

import (
	"web_db/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
	beego.Router("/all", &controllers.StudentController{}, "get:FindAllStudent")
	beego.Router("/insertStudent", &controllers.StudentController{}, "get:InsertStudent")
	beego.Router("/findStudent", &controllers.StudentController{}, "get:FindStudentById")
	beego.Router("/deleteone", &controllers.StudentController{}, "get:DeleteStudentById")
	//beego.Router("/login", &controllers.StudentController{}, "get:Login")
	//beego.Router("/getsession", &controllers.StudentController{}, "get:Getsession")
	//beego.Router("/per", &controllers.StudentController{}, "get:GetPerson")

}
