package controllers

import (
	"github.com/astaxie/beego"
	"web_db/models"
	"strconv"
)

type StudentController struct {
	beego.Controller
}

//func (s *StudentController) Login(){
//	///name := s.GetString("name")
//	s.Ctx.WriteString("qwe")
//	//s.Ctx.Redirect()
//	//name := s.Input().Get("name")
//	//s.Ctx.Input.IP()
//	//s.Ctx.SetCookie("username", name)
//	//s.Ctx.Output.Cookie("username","adsdq")
//	// //s.Ctx.SetCookie和s.Ctx.Output.Cookie一样，都是设置cookie
//
//	//s.Ctx.GetCookie("name")
//	//s.Ctx.Input.Cookie("name")
//	// //s.Ctx.GetCookie("name") 和 s.Ctx.Input.Cookie("name")一样，都是获取cookie的值
//	//session := s.GetSession("name")
//	//if session == nil {
//	//	s.SetSession("name", name)
//	//	fmt.Println("设置了")
//	//}
//
//
//	//s.Data["json"] = name
//
//	//s.ServeJSON()
//
//}


//func (s *StudentController) Getsession(){
//
//	name := s.Ctx.Input.Cookie("username")
//
//	//name := s.GetSession("name")
//	s.Data["json"] = name
//	s.ServeJSON()
//}



//// @router /all [get]
func (s *StudentController) FindAllStudent() {
 	ss := models.FindAllStudent()
 	s.Data["json"] = ss

 	s.ServeJSON()

}

//// @router /insertStudent [post]
func (s *StudentController) InsertStudent() {

	id := s.GetString("id")
	name := s.GetString("name")
	score := s.GetString("score")
	ok := models.InsertStudent(id, name,score)

	if ok == true {
		s.Data["json"] = "插入成功"
	}else {
		s.Data["json"] = "插入失败"
	}
	s.ServeJSON()

}

//// @router /findStudent [post]
func (s *StudentController) FindStudentById() {

	id := s.GetString("id")

	if stu := models.FindOneStudentById(id); stu == nil {
		s.Data["json"] = "查找失败"
	}else {
		s.Data["json"] = stu
	}
	s.ServeJSON()

}

func (s *StudentController) DeleteStudentById() {
	s_id := s.GetString("id")
	if id ,ok := models.DeleteStudentById(s_id); ok {
		s.Data["json"] = "删除成功 id: "+ strconv.Itoa(id)
	}else {
		s.Data["json"] ="删除失败"
	}
	s.ServeJSON()
}


//func (s *StudentController) GetPerson() {
//
//	p := models.FindIdPerson()
//	beego.Info(p)
//	s.Data["json"] = *p
//	s.ServeJSON()
//}

