package models

import (
	"strconv"
	"github.com/astaxie/beego/orm"
)

// 自动映射相关数据库的student表
type Student struct {
	Id int  `json:"id"; orm:"column(id)"`
	Name string `json:"name"; orm:"column(name)"`
	Score float64 `json:"score"; orm:"column(score)"`
}

// 返回所有学生
func FindAllStudent() []*Student {
	o := orm.NewOrm()
	//o.Using("qwe") // 选择数据库，不设置使用默认数据库
	var student []*Student

	o.QueryTable("student").Filter("id__gt", 4).All(&student)
	//o.Raw("select id, name, score from student").QueryRows(&student)
	return student
}

// 添加学生，如果id存在，则添加失败
func InsertStudent(s_id , name, s_score string) bool {

	id, err := strconv.Atoi(s_id)
	score, err1 := strconv.ParseFloat(s_score,64)

	if  err == nil && err1 == nil && FindOneStudentById(s_id) == nil {

		orm.NewOrm().Insert(&Student{Id:id, Name:name, Score:score})

		//o.Raw("insert into student(id,name,score) value (?,?,?)", id, name, score).Exec()
		return true
	}
	//beego.Info("用户id存在，添加失败")
	//log.Println("用户id存在，添加失败")
	return false


}

// 根据id查找学生
func FindOneStudentById(s_id string) *Student {

	id, err := strconv.Atoi(s_id);

	if  err == nil {

		stu := Student{Id:id}
		if err := orm.NewOrm().Read(&stu); err == nil {
			return &stu
		}
		//o := orm.NewOrm()
		//var student *Student
		//o.Raw("select id, name, score from student where id = ?", id).QueryRow(&student)
		//return student
	}
	return nil

}

// 根据id删除学生
func DeleteStudentById(s_id string) ( id int , ok bool) {

	if s := FindOneStudentById(s_id); s != nil {
		id , _ = strconv.Atoi(s_id)

		stu := Student{Id:id}

		if _, err := orm.NewOrm().Delete(&stu); err == nil {
			return id, true
		}
		//o := orm.NewOrm()
		//
		//o.Raw("delete from student where id = ?",id).Exec()
	}

	return -1, false

}

func init() {
	// 注册定义的model
	orm.RegisterModel(new(Student))
}

