package main

import (
	_ "web_db/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)
// 初始化mysql环境
func init() {

	orm.RegisterDriver("mysql", orm.DRMySQL) //注册驱动
	//orm.RegisterModel(new(Model))//注册 model

	// 注册自己使用的数据库
	//orm.RegisterDataBase("qwe", "mysql", "root:123@tcp(localhost:3306)/onedb?charset=utf8", 30, 30)

	// 注册默认数据库
    // mysq容器的用户名和密码，172.17.0.3为mysql容器的ip地址（可使用 cat /etc/hosts查看），对外暴露的端口为3306                                                                
	orm.RegisterDataBase("default", "mysql", "root:123456@tcp(172.17.0.3:3306)/onedb?charset=utf8", 30, 30)

	//// 拦截未登陆的任何操作，强制跳转登录页面
	//var FilterUser = func(ctx *context.Context) {
	//	ok := ctx.Input.Session("name")
	//	if ok == nil && ctx.Request.RequestURI != "/login" {
	//		ctx.Redirect(302, "/login")
	//	}
	//}
	//
	//beego.InsertFilter("/*",beego.BeforeRouter,FilterUser)
}
func main() {
	beego.Run()
}

